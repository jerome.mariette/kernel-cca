# TSKCCA
article : https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5310015/
implémentation matlab : https://github.com/kosyoshida/TSKCCA

# GWAS R packages :
* RAINBOWR : https://cran.r-project.org/web/packages/RAINBOWR/readme/README.html
* KMgene : https://cran.r-project.org/web/packages/KMgene/